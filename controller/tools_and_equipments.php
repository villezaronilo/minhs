<?php
$__APP__ = dirname(dirname(__FILE__));
// print " [From Controller: " . $__APP__ . "] ";
require_once('app.php');
require_once($__APP__.'/model/tools_and_equipments.php');

class ToolsAndEquipmentsController extends AppController {
    public $ToolsAndEquipmentsModel;

    function __construct() {
        parent::__construct();
        $this->ToolsAndEquipmentsModel = new ToolsAndEquipments();
    }

    function add(array $data): int {
        $id = $this->ToolsAndEquipmentsModel->add($data);
        return $id;
    }

    function getByReport(int $report_id, int  $limit=10, int $page=1, string $search=null, bool $all=null ): array {
        return array(
            "data"=> $this->ToolsAndEquipmentsModel->getByReport($report_id, $limit, $page, $search, $all),
            "count"=> $this->ToolsAndEquipmentsModel->getCountByReport($report_id, $search)
        );
    }

    function get(int $id=null, int  $limit=10, int $page=1, bool $all=null ): array {
        return array(
            "data"=> $this->ToolsAndEquipmentsModel->get($id, $limit, $page, $all),
            "count"=> $this->ToolsAndEquipmentsModel->getCount()
        );
    }


    function edit(int $id, array $data): int {

        $id = $this->ToolsAndEquipmentsModel->edit($id, $data);
        return $id;
    }

    function remove(int $id): int {
        $id = $this->ToolsAndEquipmentsModel->remove($id);
        return $id;
    }

}