<?php
$__APP__ = dirname(dirname(__FILE__));
// print " [From Controller: " . $__APP__ . "] ";
require_once('app.php');
require_once($__APP__.'/model/users.php');

class UsersController extends AppController {
    public $Users;

    function __construct() {
        parent::__construct();
        $this->Users = new Users();
    }

    function getLogin(array $data): array {
        $details = $this->Users->getLogin($data['username'], $data['password']);
        return $details;
    }

    function add(array $data): int {
        $id = $this->Users->add($data);
        return $id;
    }


    function get(int $id=null, int  $limit=10, int $page=1, bool $all=null ): array {
        return array(
            "data"=> $this->Users->get($id, $limit, $page, $all),
            "count"=> $this->Users->getCount()
        );
    }


    function edit(int $id, array $data): int {

        $id = $this->Users->edit($id, $data);
        return $id;
    }

    function remove(int $id): int {
        $id = $this->Users->remove($id);
        return $id;
    }

}