<?php

class AppController {
    public $APP_NAME = 'minhs';

    function __construct() {
        $this->checkSession();
    }

    private function checkSession() {
        $current_path_array = explode("/", $_SERVER['PHP_SELF']);
        $file_name = end($current_path_array);
        // Denied access to login page if already login
        if ("login.php" == $file_name) {
            if (isset($_SESSION['loginUser'])) {
                header('Location: index.php');
                return True;
            }
        }

        if (!isset($_SESSION['loginUser']) && "login.php" != $file_name) {
            print_r("test");
            $location = "Location: /{$this->APP_NAME}/login.php";
            header($location);
            return True;

        }

    }
}