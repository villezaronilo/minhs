<?php
$__APP__ = dirname(dirname(__FILE__));
// print " [From Controller: " . $__APP__ . "] ";
require_once('app.php');
require_once($__APP__.'/model/movs.php');

class MovsController extends AppController {
    public $MovsModel;

    function __construct() {
        parent::__construct();
        $this->MovsModel = new Movs();
    }

    function add(array $data, array $file_data): int {
        move_uploaded_file($file_data['filename']['tmp_name'], '../files/'.$file_data['filename']['name']);
        $data = array(
            'filename' => $file_data['filename']['name'],
            'description' => $data['description']
        );
        $id = $this->MovsModel->add($data);

        return $id;
    }


    function get(int $id=null, int  $limit=10, int $page=1, string $search=null, bool $all=null ): array {
        return array(
            "data"=> $this->MovsModel->get($id, $limit, $page, $search, $all),
            "count"=> $this->MovsModel->getCount($search)
        );
    }


    function edit(int $id, array $data, array $file_data): int {
        move_uploaded_file($file_data['filename']['tmp_name'], '../files/'.$file_data['filename']['name']);
        $data = array(
            'filename' => $file_data['filename']['name'],
            'description' => $data['description']
        );
        $id = $this->MovsModel->edit($id, $data);
        return $id;
    }

    function remove(int $id): int {
        $id = $this->MovsModel->remove($id);
        return $id;
    }

}