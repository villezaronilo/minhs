<?php
$__APP__ = dirname(dirname(__FILE__));
// print " [From Controller: " . $__APP__ . "] ";
require_once('app.php');
require_once($__APP__.'/model/reports.php');

class ReportsController extends AppController {
    public $ReportsModel;

    function __construct() {
        parent::__construct();
        $this->ReportsModel = new Reports();
    }

    function add(array $data): int {
        $id = $this->ReportsModel->add($data);
        return $id;
    }


    function get(int $id=null, int  $limit=10, int $page=1, string $search=null, bool $all=null ): array {
        return array(
            "data"=> $this->ReportsModel->get($id, $limit, $page, $search, $all),
            "count"=> $this->ReportsModel->getCount($search)
        );
    }


    function edit(int $id, array $data): int {

        $id = $this->ReportsModel->edit($id, $data);
        return $id;
    }

    function remove(int $id): int {
        $id = $this->ReportsModel->remove($id);
        return $id;
    }

}