<?php
$__APP__ = dirname(dirname(__FILE__));
// print " [From Model: " . $__APP__. "] ";
require_once($__APP__.'/model/base.php');

class ToolsAndEquipments extends BaseModel {
    private $__table__ = 'tools_and_equipments';
    private $__fields__ = array(
        "id",
        "report_id",
        "article",
        "description",
        "property_number",
        "unit_of_measure",
        "unit_value",
        "quantity_per_property_card",
        "quantity_per_physical_count",
        "shortage_coverage_quantity",
        "shortage_coverage_value",
        "remarks"
    );
    
    
    function add(array $data ): int {
        return $this->save($this->__table__, $data);
    }

    function remove(int $id ): int {
        return $this->delete($this->__table__, $id);
    }

    function getCount(): int {
        return $this->readCount($this->__table__);

    }
    
    function getCountByReport(int $report_id, string $search=null): int {
        $sql = "SELECT * FROM `{$this->__table__}` ";
        $sql .= "WHERE is_deleted = false ";
        $sql .= "AND report_id = {$report_id} ";

        if ($search) {
            $sql .= "AND (`article` COLLATE UTF8_GENERAL_CI LIKE '%{$search}%' ";
            $sql .= "OR `description` COLLATE UTF8_GENERAL_CI LIKE '%{$search}%' ";
            $sql .= "OR `property_number` COLLATE UTF8_GENERAL_CI LIKE '%{$search}%') ";
        }

        $result = $this->mysqli->query($sql);
        if ($result) {
            return $result->num_rows;
        }
        return 0;
    }

    function getByReport(int $id=null, int $items_per_page=10, int $page=1, string $search=null, bool $all=null): array {
        $offset = ($page - 1) * $items_per_page;

        $sql = "SELECT * FROM `{$this->__table__}` ";
        $sql .= "WHERE is_deleted = false ";
        $sql .= "AND report_id = {$id} ";
        
        if ($search) {
            $sql .= "AND (`article` COLLATE UTF8_GENERAL_CI LIKE '%{$search}%' ";
            $sql .= "OR `description` COLLATE UTF8_GENERAL_CI LIKE '%{$search}%' ";
            $sql .= "OR `property_number` COLLATE UTF8_GENERAL_CI LIKE '%{$search}%') ";
        }
        
        // For pagination
        if ($all != true) {
            $sql .= "limit {$items_per_page} ";
            $sql .= "offset {$offset} ";
        }
        // echo "<pre>";
        // print_r($sql);
        // echo "</pre>";
        $result = $this->mysqli->query($sql);
        if ($result) {
            return $result->fetch_all(MYSQLI_ASSOC);
        }
        return array();

    }

    function get(int $id=null, int $items_per_page=10, int $page=1, bool $all=null): array {
        return $this->read(
            $this->__table__,
            $id,
            $items_per_page,
            $page,
            $all
            );

    }

    function edit(int $id, array $data) {
        return $this->update(
            $this->__table__,
            $id,
            $data);
    }

}
