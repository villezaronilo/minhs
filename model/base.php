<?php
class BaseModel {
    public $mysqli;
    
    function __construct() {
        // $mysqli = new mysqli("<host>", "<username>", "<password>", "<database_name>");
        // print "In constructor\n";
        // Database connection
        $this->mysqli = new mysqli("localhost", "minhs_user", "minhs_password", "minhs_db");
        
        // Check connection
        if ($this->mysqli->connect_errno) {
            echo "Failed to connect to MySQL: " . $this->mysqli->connect_error;
            exit();
        }
    }

    function __destruct() {
        // This function will be called to close the database connection.
        // print "Destroying " . __CLASS__ . "\n";
        $this->mysqli->close();
    }

    function escape_mysqli_string($value) {
        // Escape Special character.
        return $this->mysqli->real_escape_string($value);
    }

    function save(string $table_name, array $data): int {
        $columns = "`".implode("`, `",array_keys($data))."`";
        $escaped_values = array_map(array($this, 'escape_mysqli_string'), array_values($data));
        $values  = '"'.implode('", "', $escaped_values).'"';
        $sql = "INSERT INTO `{$table_name}`({$columns}) VALUES ({$values})";

        $this->mysqli->query($sql);
        return $this->mysqli->insert_id;
    }

    function read(string $table_name, int $id=null, int $items_per_page=10, int $page=1, bool $all=null): array {
        $offset = ($page - 1) * $items_per_page;

        $sql = "SELECT * FROM `{$table_name}` ";
        $sql .= "WHERE is_deleted = false ";
        if ($id)
            $sql .= "AND id = {$id} ";
        
        // For pagination
        if ($all != true) {
            $sql .= "limit {$items_per_page} ";
            $sql .= "offset {$offset} ";
        }
        // echo "<pre>";
        // print_r($sql);
        // echo "</pre>";
        $result = $this->mysqli->query($sql);
        if ($result) {
            return $result->fetch_all(MYSQLI_ASSOC);
        }
        return array();
    }
    
    function readCount(string $table_name): int {
        $sql = "SELECT * FROM `{$table_name}` ";
        $sql .= "WHERE is_deleted = false ";
        $result = $this->mysqli->query($sql);
        if ($result) {
            return $result->num_rows;
        }
        return 0;
    }

    function prepare_update_fields(array $data): array {
        $array_set_fields = array();
        foreach ($data as $key => $val) {
            $array_set_fields[] = "`{$key}`='{$this->escape_mysqli_string($val)}'";
        }
        return $array_set_fields;
    }

    function update(string $table_name, int $id, array $data): int {
        $update_fields = $this->prepare_update_fields($data);
        $fields_values = implode(", ", $update_fields);
        $sql = "UPDATE `{$table_name}` ";
        $sql .= "SET {$fields_values} ";
        $sql .= "WHERE id = {$id} ";
        // echo "<pre>";
        // echo $sql;
        // echo "</pre>";
        $this->mysqli->query($sql);
        $affected_row = $this->mysqli->affected_rows;
        // echo $affected_row;
        if (0 <= $affected_row)
            return $id;
        return 0;
    }

    function delete(string $table_name, int $id): int {
        // We only do soft deleted incase we need to recover data
        $sql = "UPDATE `{$table_name}` ";
        $sql .= "SET `is_deleted`=true ";
        $sql .= "WHERE id = {$id} ";
        $this->mysqli->query($sql);
        $affected_row = $this->mysqli->affected_rows;
        // echo $affected_row;
        if (0 <= $affected_row)
            return $id;
        return 0;
    }

}
