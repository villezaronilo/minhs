<?php
$__APP__ = dirname(dirname(__FILE__));
// print " [From Model: " . $__APP__. "] ";
require_once($__APP__.'/model/base.php');

class Movs extends BaseModel {
    private $__table__ = 'movs';
    private $__fields__ = array(
        "id",
        "filename",
        "description"
    );
    
    function add(array $data ): int {
        return $this->save($this->__table__, $data);
    }

    function remove(int $id ): int {
        return $this->delete($this->__table__, $id);
    }

    function getCount(string $search=null): int {
        $sql = "SELECT id FROM `{$this->__table__}` ";
        $sql .= "WHERE is_deleted = false ";
        if ($search) {
            $sql .= "AND `filename` COLLATE UTF8_GENERAL_CI LIKE '%{$search}%' ";
        }

        $result = $this->mysqli->query($sql);
        if ($result) {
            return $result->num_rows;
        }
        return 0;

    }

    function get(int $id=null, int $items_per_page=10, int $page=1, string $search=null, bool $all=null): array {
        $offset = ($page - 1) * $items_per_page;

        $sql = "SELECT * FROM `{$this->__table__}` ";
        $sql .= "WHERE is_deleted = false ";
        if ($id)
            $sql .= "AND id = {$id} ";
        
        if ($search) {
            $sql .= "AND `filename` COLLATE UTF8_GENERAL_CI LIKE '%{$search}%' ";
        }
        
        // For pagination
        if ($all != true) {
            $sql .= "limit {$items_per_page} ";
            $sql .= "offset {$offset} ";
        }
        // echo "<pre>";
        // print_r($sql);
        // echo "</pre>";
        $result = $this->mysqli->query($sql);
        if ($result) {
            return $result->fetch_all(MYSQLI_ASSOC);
        }
        return array();
    }

    function edit(int $id, array $data) {
        return $this->update(
            $this->__table__,
            $id,
            $data);
    }

}
