<?php
$__APP__ = dirname(dirname(__FILE__));
// print " [From Model: " . $__APP__. "] ";
require_once($__APP__.'/model/base.php');

class Users extends BaseModel {
    private $__table__ = 'users';
    private $__fields__ = array(
        "id",
        "username",
        "password",
        "role"
    );
    
    function getLogin(string $username, string $password){
        $sql = "SELECT * FROM `{$this->__table__}` ";
        $sql .= "WHERE username = '{$username}' ";
        $sql .= "and password = '{$password}' ";
        $result = $this->mysqli->query($sql);
        if ($result) {
            return $result->fetch_all(MYSQLI_ASSOC);
        }
        return array();
    }
    
    function add(array $data ): int {
        return $this->save($this->__table__, $data);
    }

    function remove(int $id ): int {
        return $this->delete($this->__table__, $id);
    }

    function getCount(): int {
        return $this->readCount($this->__table__);

    }

    function get(int $id=null, int $items_per_page=10, int $page=1, bool $all=null): array {
        return $this->read(
            $this->__table__,
            $id,
            $items_per_page,
            $page,
            $all
            );

    }

    function edit(int $id, array $data) {
        return $this->update(
            $this->__table__,
            $id,
            $data);
    }

}
