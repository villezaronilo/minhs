<nav class="navbar navbar-expand-md navbar-dark bg-dark fixed-top">
  <a class="navbar-brand" href="#">MINHS</a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarsExampleDefault" aria-controls="navbarsExampleDefault" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>

  <div class="collapse navbar-collapse" id="navbarsExampleDefault">
    <ul class="navbar-nav mr-auto">
      <li class="nav-item">
        <a class="nav-link" href="/<?php echo APP_NAME;?>">Home</a>
      </li>
      <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle" href="#" id="dropdown01" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Leadership and Governance</a>
        <div class="dropdown-menu" aria-labelledby="dropdown01">
          <a class="dropdown-item" href="/<?php echo APP_NAME;?>/movs">Planning</a>
          <a class="dropdown-item" href="#">Reviewing</a>
          <a class="dropdown-item" href="#">Organizing</a>
          <a class="dropdown-item" href="#">Facilitating</a>
          <a class="dropdown-item" href="#">Operating</a>
        </div>
      </li>
      <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle" href="#" id="dropdown01" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Management of Resources</a>
        <div class="dropdown-menu" aria-labelledby="dropdown01">
          <a class="dropdown-item" href="/<?php echo APP_NAME;?>/reports">Resource Inventory</a>
          <a class="dropdown-item" href="#">Regular Dialogue</a>
          <a class="dropdown-item" href="#">Resource Management</a>
          <a class="dropdown-item" href="#">Regular Monitor</a>
          <a class="dropdown-item" href="#">Manage Linkages</a>
        </div>
      </li>
      <?php if (isset($_SESSION['loginUser'])): ?>
        <li class="nav-item">
        <a class="nav-link" href="/<?php echo APP_NAME;?>/logout.php">Logout</a>
      </li>
      <?php else: ?>
      <li class="nav-item">
        <a class="nav-link" href="/<?php echo APP_NAME;?>/login.php">Login</a>
      </li>
      <?php endif; ?>
    </ul>
  </div>
</nav>