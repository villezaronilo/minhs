<?php
session_start();
require_once('constant.php');
$_SESSION = array();
session_destroy();
$location = "Location: login.php";
header($location);
?>