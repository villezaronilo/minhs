<?php
session_start();
    // print " [From View: " . $__APP__ ."] ";
    require_once('../constant.php');
    require_once('../controller/movs.php');

    $MovsController = new MovsController();
    // echo "<pre>";
    // print_r($_POST);
    // echo "</pre>";
    // echo "<pre>";
    // print_r($_FILES);
    // echo "</pre>";
    if (isset($_POST['submit'])) {
        unset($_POST['submit']);
        $id = $MovsController->add($_POST, $_FILES);
        if (0 <= $id) {
            $location = "Location: ../movs/edit.php?id={$id}";
            header($location);
        }
    }
   
    require_once('../header.php');
    require_once('../header-end.php');
    require_once('../navigation.php');
?>
<main role="main" class="container">

<form method="POST" enctype="multipart/form-data">
    <div class="form-group">
        <label for="filenameInput">File</label>
        <input name="filename" type="file" class="form-control" id="filenameInput">
    </div>
    <div class="form-group">
        <label for="descriptionTextArea">Description</label>
        <textarea name="description" class="form-control" id="descriptionTextArea" rows="3"></textarea>
    </div>
    <button name="submit" type="submit" class="btn btn-primary">Submit</button>
    <a class="btn btn-primary" href="../movs" role="button">Cancel</a>
</form>


</main><!-- /.container -->
<?php
    require_once('../footer.php');
