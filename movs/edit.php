<?php
session_start();
    // print " [From View: " . $__APP__ ."] ";
    require_once('../constant.php');
    require_once('../controller/movs.php');

    $MovsController = new MovsController();
    // $MovsController->add();
    // echo "<pre>";
    // print_r($_POST);
    // print_r($_GET);
    // echo "</pre>";
    if (isset($_POST['submit'])) {
        unset($_POST['submit']);
        $id = $MovsController->edit($_GET['id'], $_POST, $_FILES);
        if (0 <= $id) {
            // header('Location: ../movs?success=true');
        }
    }

    $details = $MovsController->get((int)$_GET['id']);
    if (empty($details)) {
        header('Location: ../tools-and-equipments');
    }
    $detail = $details['data'][0];
   
    require_once('../header.php');
    require_once('../header-end.php');
    require_once('../navigation.php');
?>
<main role="main" class="container">
<form method="POST" enctype="multipart/form-data" action="?id=<?php echo $_GET['id']; ?>">
    <div class="form-group">
        <label for="filenameInput">File</label>
        <input name="filename" type="file" class="form-control" id="filenameInput">
        <a href="../files/<?php echo $detail['filename'];?>"><?php echo $detail['filename'];?></a>
    </div>
    <div class="form-group">
        <label for="descriptionTextArea">Description</label>
        <textarea name="description" class="form-control" id="descriptionTextArea" rows="3"><?php echo $detail['description'];?></textarea>
    </div>
    <button name="submit" type="submit" class="btn btn-primary">Submit</button>
    <a class="btn btn-primary" href="../movs" role="button">Cancel</a>
</form>

</main><!-- /.container -->
<?php
    require_once('../footer.php');
