<?php
session_start();
    // print " [From View: " . $__APP__ ."] ";
    require_once('../constant.php');
    require_once('../controller/reports.php');

    $ReportsController = new ReportsController();
    // $ReportsController->add();
    // echo "<pre>";
    // print_r($_POST);
    // print_r($_GET);
    // echo "</pre>";
    if (isset($_POST['submit'])) {
        unset($_POST['submit']);
        $id = $ReportsController->remove($_GET['id']);
        if (0 <= $id) {
            header("Location: ../reports?deleted=true");
        }
    }

    $details = $ReportsController->get((int)$_GET['id']);
    if (empty($details)) {
        header('Location: ../reports');
    }
    $detail = $details['data'][0];
   
    require_once('../header.php');
    require_once('../header-end.php');
    require_once('../navigation.php');
?>
<main role="main" class="container">

<span class="badge badge-danger">Are you sure you want to DELETE this data?</span>
<form actoin="?id=<?php echo $_GET['id']; ?>" method="POST">
<div class="form-group">
        <label for="funcClusterInput">Report Name</label>
        <input disabled disabled name="fund_cluster" type="text" class="form-control" id="funcClusterInput" value="<?php echo $detail['fund_cluster'];?>">
    </div>
    <div class="form-group">
        <label for="nameInput">Report Name</label>
        <input disabled name="name" type="text" class="form-control" id="nameInput" value="<?php echo $detail['name'];?>">
    </div>
    <div class="form-group">
        <label for="accountOfficerInput">Account Officer</label>
        <input disabled name="accountable_officer" type="text" class="form-control" id="accountOfficerInput" value="<?php echo $detail['accountable_officer'];?>">
    </div>
    <div class="form-group">
        <label for="officerDesignationInput">Officer Designation</label>
        <input disabled name="officer_designation" type="text" class="form-control" id="officerDesignationInput" value="<?php echo $detail['officer_designation'];?>">
    </div>
    <div class="form-group">
        <label for="agencyInput">Agency / Office</label>
        <input disabled name="agency" type="text" class="form-control" id="agencyInput" value="<?php echo $detail['agency'];?>">
    </div>
    <div class="form-group">
        <label for="dateAssumptionInput">Date Assumption</label>
        <input disabled name="date_assumption" type="text" class="form-control" id="dateAssumptionInput" value="<?php echo $detail['date_assumption'];?>">
    </div>
    <div class="form-group">
        <label for="certifiedNameInput">Certified Correct by</label>
        <input disabled name="certified_name" type="text" class="form-control" id="certifiedNameInput" value="<?php echo $detail['certified_name'];?>">
    </div>
    <div class="form-group">
        <label for="certifiedDesignationInput">Certified personnel designation</label>
        <input disabled name="certified_designation" type="text" class="form-control" id="certifiedDesignationInput" value="<?php echo $detail['certified_designation'];?>">
    </div>
    <div class="form-group">
        <label for="approvedNameInput">Approved by</label>
        <input disabled name="approved_name" type="text" class="form-control" id="approvedNameInput" value="<?php echo $detail['approved_name'];?>">
    </div>
    <div class="form-group">
        <label for="approvedDesignationInput">Approved by designation</label>
        <input disabled name="approved_designation" type="text" class="form-control" id="approvedDesignationInput" value="<?php echo $detail['approved_designation'];?>">
    </div>
    <div class="form-group">
        <label for="descriptionTextArea">Description</label>
        <textarea disabled name="description" class="form-control" id="descriptionTextArea" rows="3"><?php echo $detail['description'];?></textarea>
    </div>
    <button name="submit" type="submit" class="btn btn-primary">Delete</button>
    <a class="btn btn-primary" href="../reports" role="button">Cancel</a>
</form>


</main><!-- /.container -->
<?php
    require_once('../footer.php');
