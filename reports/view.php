<?php
session_start();
    // print " [From View: " . $__APP__ ."] ";
    require_once('../constant.php');
    require_once('../controller/reports.php');
    require_once('../controller/tools_and_equipments.php');

    $ToolsAndEquipmentsController = new ToolsAndEquipmentsController();
    $ReportsController = new ReportsController();

    $report_id = (int)$_GET['id'];
    $search = (empty($_GET['search'])) ? "" : (string) $_GET['search'];
    $limit = (empty($_GET['limit'])) ? 10 : (int) $_GET['limit'];
    $page = (empty($_GET['page'])) ? 1 : (int) $_GET['page'];
    // $ReportsController->add();
    // echo "<pre>";
    // print_r($_POST);
    // echo "</pre>";
    $report_details = $ReportsController->get($report_id);
    if (empty($report_details)) {
        header('Location: ../reports');
    }
    $report_detail = $report_details['data'][0];


    $lists = $ToolsAndEquipmentsController->getByReport($report_id, $limit, $page, $search);
    $item_count = $lists['count'];

    // getting pageination
    $page_count = $item_count / $limit;
    $page_modulus = $item_count % $limit;
    $page_count = (0 < $page_modulus) ? (int)$page_count + 1: $page_count;
   
    require_once('../header.php');
    require_once('../header-end.php');
    require_once('../navigation.php');
    
?>
<main role="main" class="container">
<a class="btn btn-primary" href="pdf.php?report-id=<?php echo $report_detail['id'];?>" role="button">Download PDF</a>
<table class="table table-striped">
  <thead class="thead-dark">
    <tr>
        <th scope="col">Report Name</th>
        <th scope="col">Accoutable Officer</th>
        <th scope="col">Officer Designation</th>
        <th scope="col">Agency / Office</th>
        <th scope="col">Date Assumption</th>
        <th scope="col">Certified by</th>
        <th scope="col">Approved by</th>
        <th scope="col">Description</th>
        <th scope="col">Action</th>
    </tr>
  </thead>
  <tbody>
    <tr>
        <td><?php echo $report_detail['name']; ?></td>
        <td><?php echo $report_detail['accountable_officer']; ?></td>
        <td><?php echo $report_detail['officer_designation']; ?></td>
        <td><?php echo $report_detail['agency']; ?></td>
        <td><?php echo $report_detail['date_assumption']; ?></td>
        <td><?php echo $report_detail['certified_name']; ?></td>
        <td><?php echo $report_detail['approved_name']; ?></td>
        <td><?php echo $report_detail['description']; ?></td>
        <td>
            <a href="edit.php?id=<?php echo $report_detail['id'];?>">Edit</a> | 
            <a href="delete.php?id=<?php echo $report_detail['id'];?>">Delete</a>
        </td>
    </tr>
  </tbody>
</table>
<a class="btn btn-primary" href="../tools-and-equipments/add.php?report-id=<?php echo $report_id; ?>" role="button">Add Item</a>
<form method="GET">
<input type="text" name="search" value="<?php echo $search; ?>">
<input type="hidden" name="id" value="<?php echo $report_id; ?>">
<input type="hidden" name="limit" value="<?php echo $limit; ?>">
<input type="hidden" name="page" value="<?php echo $page; ?>">
<button type="submit" class="btn btn-primary">Search</button>
</form>
<table class="table table-striped">
  <thead class="thead-dark">
    <tr>
        <th scope="col">Article</th>
        <th scope="col">Description</th>
        <th scope="col">Property Number</th>
        <th scope="col">Unit of Measure</th>
        <th scope="col">Unit Value</th>
        <th scope="col">Quantity per Property Card</th>
        <th scope="col">Quantity per Physical Count</th>
        <th scope="col">Shortage Coverage Quantity</th>
        <th scope="col">Shortage Coverage Value</th>
        <th scope="col">Remarks</th>
        <th scope="col">Action</th>
    </tr>
  </thead>
  <tbody>

    <?php foreach ($lists['data'] as $list): ?>
    <tr>
        <td><?php echo $list['article']; ?></td>
        <td><?php echo $list['description']; ?></td>
        <td><?php echo $list['property_number']; ?></td>
        <td><?php echo $list['unit_of_measure']; ?></td>
        <td><?php echo number_format($list['unit_value'], 2); ?></td>
        <td><?php echo $list['quantity_per_property_card']; ?></td>
        <td><?php echo $list['quantity_per_physical_count']; ?></td>
        <td><?php echo $list['shortage_coverage_quantity']; ?></td>
        <td><?php echo $list['shortage_coverage_value']; ?></td>
        <td><?php echo $list['remarks']; ?></td>
        <td>
            <a href="../tools-and-equipments/edit.php?report-id=<?php echo $report_id; ?>&id=<?php echo $list['id'];?>">Edit</a>
            <a href="../tools-and-equipments/delete.php?report-id=<?php echo $report_id; ?>&id=<?php echo $list['id'];?>">Delete</a>
        </td>
    </tr>
    <?php endforeach; ?>

  </tbody>
</table>
<nav aria-label="Page navigation example">
  <ul class="pagination">
    <?php for ($i=1;$i<=$page_count;$i++): ?>
        <li class="page-item"><a class="page-link" href="?search=<?php echo $search; ?>&id=<?php echo $report_id; ?>&page=<?php echo $i; ?>&limit=<?php echo $limit; ?>"><?php echo $i; ?></a></li>
    <?php endfor; ?>
  </ul>
</nav>

</main><!-- /.container -->
<?php
    require_once('../footer.php');
