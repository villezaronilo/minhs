<?php
session_start();

error_reporting(0);
ini_set('display_errors', 0);

require_once('../constant.php');
require __DIR__.'/../vendor/autoload.php';
require_once(__DIR__.'/../controller/tools_and_equipments.php');
require_once(__DIR__.'/../controller/reports.php');


use Spipu\Html2Pdf\Html2Pdf;
$ToolsAndEquipmentsController = new ToolsAndEquipmentsController();
$ReportsController = new ReportsController();
$report_id = $_GET['report-id'];

$report_details = $ReportsController->get($report_id);
$lists = $ToolsAndEquipmentsController->getByReport($report_id, 1, 1, "", true);

// echo "<pre>";
// print_r($report_details);
// echo "</pre>";
// echo "<pre>";
// print_r($lists);
// echo "</pre>";

ob_start();
$report_details = $ReportsController->get($report_id);
$lists = $ToolsAndEquipmentsController->getByReport($report_id, 1, 1, "", true);
#$lists = $ToolsAndEquipmentsController->get(null, 1, 1, true);
require dirname(__FILE__).'/../template/pdf/reports.php';
$content = ob_get_clean();

$html2pdf = new Html2Pdf('L', 'A4', 'en');
$html2pdf->pdf->SetDisplayMode('fullpage');
// $html2pdf->writeHTML('<h1>HelloWorld</h1>This is my first test');
$html2pdf->writeHTML($content);
# $html2pdf->output("test.pdf", "D");
$html2pdf->output("test.pdf");