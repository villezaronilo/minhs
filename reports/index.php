<?php
session_start();
    // print " [From View: " . $__APP__ ."] ";
    require_once('../constant.php');
    require_once('../controller/reports.php');

    $ReportsController = new ReportsController();
    $search = (empty($_GET['search'])) ? "" : (string) $_GET['search'];
    $limit = (empty($_GET['limit'])) ? 10 : (int) $_GET['limit'];
    $page = (empty($_GET['page'])) ? 1 : (int) $_GET['page'];
    // echo "<pre>";
    // print_r($_GET['limit']);
    // print_r($limit);
    // echo "</pre>";

    $lists = $ReportsController->get(null, $limit, $page, $search);
    $item_count = $lists['count'];

    // getting pageination
    $page_count = $item_count / $limit;
    $page_modulus = $item_count % $limit;
    $page_count = (0 < $page_modulus) ? (int)$page_count + 1: $page_count;

    require_once('../header.php');
    require_once('../header-end.php');
    require_once('../navigation.php');
?>
<main role="main" class="container">

<a class="btn btn-primary" href="add.php" role="button">Add Report</a>
<form method="GET">
<input type="text" name="search" value="<?php echo $search; ?>">
<input type="hidden" name="limit" value="<?php echo $limit; ?>">
<input type="hidden" name="page" value="<?php echo $page; ?>">
<button type="submit" class="btn btn-primary">Search</button>
</form>
<table class="table table-striped">
  <thead class="thead-dark">
    <tr>
        <th scope="col">Report Name</th>
        <th scope="col">Accoutable Officer</th>
        <th scope="col">Officer Designation</th>
        <th scope="col">Agency / Office</th>
        <th scope="col">Date Assumption</th>
        <th scope="col">Description</th>
        <th scope="col">Action</th>
    </tr>
  </thead>
  <tbody>
    <?php foreach ($lists['data'] as $list): ?>
    <tr>
        <td><?php echo "{$list['name']}"; ?></td>
        <td><?php echo "{$list['accountable_officer']}"; ?></td>
        <td><?php echo "{$list['officer_designation']}"; ?></td>
        <td><?php echo "{$list['agency']}"; ?></td>
        <td><?php echo "{$list['date_assumption']}"; ?></td>
        <td><?php echo "{$list['description']}"; ?></td>
        <td>
            <a href="view.php?id=<?php echo $list['id'];?>">View</a> | 
            <a href="edit.php?id=<?php echo $list['id'];?>">Edit</a> | 
            <a href="delete.php?id=<?php echo $list['id'];?>">Delete</a>
        </td>
    </tr>
    <?php endforeach; ?>

  </tbody>
</table>
<nav aria-label="Page navigation example">
  <ul class="pagination">
    <?php for ($i=1;$i<=$page_count;$i++): ?>
        <li class="page-item"><a class="page-link" href="?search=<?php echo $search; ?>&page=<?php echo $i; ?>&limit=<?php echo $limit; ?>"><?php echo $i; ?></a></li>
    <?php endfor; ?>
  </ul>
</nav>


</main><!-- /.container -->
<?php
    require_once('../footer.php');
