<?php
session_start();
    // print " [From View: " . $__APP__ ."] ";
    require_once('../constant.php');
    require_once('../controller/reports.php');

    $ReportsController = new ReportsController();
    // $ReportsController->add();
    // echo "<pre>";
    // print_r($_POST);
    // echo "</pre>";
    if (isset($_POST['submit'])) {
        unset($_POST['submit']);
        $id = $ReportsController->add($_POST);
        if (0 <= $id) {
            $location = "Location: ../reports/edit.php?id={$id}";
            header($location);
        }
    }
   
    require_once('../header.php');
    require_once('../header-end.php');
    require_once('../navigation.php');
?>
<main role="main" class="container">

<form method="POST">
    <div class="form-group">
        <label for="funcClusterInput">Fund Cluster</label>
        <input name="fund_cluster" type="text" class="form-control" id="funcClusterInput">
    </div>
    <div class="form-group">
        <label for="nameInput">Report Name</label>
        <input name="name" type="text" class="form-control" id="nameInput">
    </div>
    <div class="form-group">
        <label for="accountOfficerInput">Account Officer</label>
        <input name="accountable_officer" type="text" class="form-control" id="accountOfficerInput">
    </div>
    <div class="form-group">
        <label for="officerDesignationInput">Officer Designation</label>
        <input name="officer_designation" type="text" class="form-control" id="officerDesignationInput">
    </div>
    <div class="form-group">
        <label for="agencyInput">Agency / Office</label>
        <input name="agency" type="text" class="form-control" id="agencyInput">
    </div>
    <div class="form-group">
        <label for="dateAssumptionInput">Date Assumption</label>
        <input name="date_assumption" type="text" class="form-control" id="dateAssumptionInput">
    </div>
    <div class="form-group">
        <label for="certifiedNameInput">Certified Correct by</label>
        <input name="certified_name" type="text" class="form-control" id="certifiedNameInput">
    </div>
    <div class="form-group">
        <label for="certifiedDesignationInput">Certified personnel designation</label>
        <input name="certified_designation" type="text" class="form-control" id="certifiedDesignationInput">
    </div>
    <div class="form-group">
        <label for="approvedNameInput">Approved by</label>
        <input name="approved_name" type="text" class="form-control" id="approvedNameInput">
    </div>
    <div class="form-group">
        <label for="approvedDesignationInput">Approved by designation</label>
        <input name="approved_designation" type="text" class="form-control" id="approvedDesignationInput">
    </div>
    <div class="form-group">
        <label for="descriptionTextArea">Description</label>
        <textarea name="description" class="form-control" id="descriptionTextArea" rows="3"></textarea>
    </div>
    <button name="submit" type="submit" class="btn btn-primary">Submit</button>
    <a class="btn btn-primary" href="../tools-and-equipments" role="button">Cancel</a>
</form>


</main><!-- /.container -->
<?php
    require_once('../footer.php');
