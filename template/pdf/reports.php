<style type="text/css">
<!--
table th, td
{
    text-align: center;
    padding: 2px;
}

-->
</style>

<page backcolor="#FEFEFE" backimgx="center" backimgy="bottom" backimgw="100%" backtop="0" backbottom="30mm" footer="date;time;page" style="font-size: 9pt">
<table style="width: 100%;" align="center" border=0>
    <tr>
        <td colspan="7">
            REPORT ON THE PHYSICAL COUNT OF TOOLS AND EQUIPMENT										
        </td>
    </tr>
    <tr>
        <td colspan="7">
            As of <?php echo date("F d Y"); ?>
        </td>
    </tr>
    <tr>
        <td colspan="7">
            <?php echo $report_details['data'][0]['name']; ?>
        </td>
    </tr>
    <tr>
        <td style="width: 10%; text-align: left;">
            Fund Cluster
        </td>
        <td style="width: 80%; text-align: left;" colspan="6">
            <strong><?php echo $report_details['data'][0]['fund_cluster']; ?></strong>
        </td>
    </tr>
    <tr>
        <td style="text-align: left;">
            For which
        </td>
        <td style="text-align: left;">
            <strong><?php echo $report_details['data'][0]['accountable_officer']; ?></strong>
        </td>
        <td style="text-align: left;">
            <strong><?php echo $report_details['data'][0]['officer_designation']; ?></strong>
        </td>
        <td style="text-align: left;">
            of
        </td>
        <td style="text-align: left;">
            <strong><?php echo $report_details['data'][0]['agency']; ?></strong>
        </td>
        <td style="text-align: left;">
            is accountable, having assumed such accountability on
        </td>
        <td style="text-align: left;">
            <strong><?php echo $report_details['data'][0]['date_assumption']; ?></strong>
        </td>
    </tr>
    <tr>
        <td>
        </td>
        <td style="text-align: left;">
            (Name of accountable Officer)
        </td>
        <td style="text-align: left;">
            (Official Designation)
        </td>
        <td>
        </td>
        <td style="text-align: left;">
            (Agency / Office)
        </td>
        <td>
        </td>
        <td style="text-align: left;">
            (Date of Assumption)
        </td>
    </tr>
</table>
<table style="width: 100%;" align="center" border=1>
<tr style="height: 50px;">
    <th style="width: 10%;" rowspan="2">Article</th>
    <th style="width: 10%;" rowspan="2">Description</th>
    <th style="width: 10%;" rowspan="2">Property Number</th>
    <th style="width: 10%;" rowspan="2">Unit of Measure</th>
    <th style="width: 10%;" rowspan="2">Unit Value</th>
    <th style="width: 10%;" colspan="2">Quantity per</th>
    <th style="width: 10%;" colspan="2">Shortage Coverage</th>
    <th style="width: 10%;" rowspan="2">Remarks</th>
</tr>
<tr>
    <th>Property Card</th>
    <th>Physical Count</th>
    <th>Quantity</th>
    <th>Value</th>
</tr>
<?php foreach ($lists['data'] as $list): ?>
<tr>
    <td><?php echo $list['article']; ?></td>
    <td><?php echo $list['description']; ?></td>
    <td><?php echo $list['property_number']; ?></td>
    <td><?php echo $list['unit_of_measure']; ?></td>
    <td><?php echo $list['unit_value']; ?></td>
    <td><?php echo $list['quantity_per_property_card']; ?></td>
    <td><?php echo $list['quantity_per_physical_count']; ?></td>
    <td><?php echo $list['shortage_coverage_quantity']; ?></td>
    <td><?php echo $list['shortage_coverage_value']; ?></td>
    <td><?php echo $list['remarks']; ?></td>
</tr>
<?php endforeach; ?>
</table>
<table style="width: 100%;" align="center" border=0>
    <tr>
        <td style="width: 30%; text-align: left;">
            Certified Correct by:
        </td>
        <td style="width: 30%; text-align: left;">
            Approved by:
        </td>
        <td style="width: 30%; text-align: left;">
            Verified by:
        </td>
    </tr>
    <tr>
        <td style="height: 25px;">
        </td>
        <td>
        </td>
        <td>
        </td>
    </tr>
    <tr>
        <td style="text-align: left;">
            <strong><?php echo $report_details['data'][0]['certified_name']; ?></strong>
        </td>
        <td style="text-align: left;">
            <strong><?php echo $report_details['data'][0]['approved_name']; ?></strong>
        </td>
        <td style="text-align: left;">
                __________________________________________
        </td>
    </tr>
    <tr>
        <td style="text-align: left;">
            <?php echo $report_details['data'][0]['certified_designation']; ?>
        </td>
        <td style="text-align: left;">
            <?php echo $report_details['data'][0]['approved_designation']; ?>
        </td>
        <td style="text-align: left;">
                Signature over printed name of COA Representative
        </td>
    </tr>
</table>
</page>
