<style type="text/css">
<!--
table th, td
{
    text-align: center;
    padding: 2px;
}

-->
</style>

<page backcolor="#FEFEFE" backimgx="center" backimgy="bottom" backimgw="100%" backtop="0" backbottom="30mm" footer="date;time;page" style="font-size: 9pt">
<table style="width: 100%;" align="center" border=0>
    <tr>
        <td>
            REPORT ON THE PHYSICAL COUNT OF TOOLS AND EQUIPMENT										
        </td>
    </tr>
</table>
<table style="width: 100%;" align="center" border=1>
<tr style="height: 50px;">
    <th style="width: 10%;" rowspan="2">Article</th>
    <th style="width: 10%;" rowspan="2">Description</th>
    <th style="width: 10%;" rowspan="2">Property Number</th>
    <th style="width: 10%;" rowspan="2">Unit of Measure</th>
    <th style="width: 10%;" rowspan="2">Unit Value</th>
    <th style="width: 10%;" colspan="2">Quantity per</th>
    <th style="width: 10%;" colspan="2">Shortage Coverage</th>
    <th style="width: 10%;" rowspan="2">Remarks</th>
</tr>
<tr>
    <th>Property Card</th>
    <th>Physical Count</th>
    <th>Quantity</th>
    <th>Value</th>
</tr>
<?php foreach ($lists['data'] as $list): ?>
<tr>
    <td><?php echo $list['article']; ?></td>
    <td><?php echo $list['description']; ?></td>
    <td><?php echo $list['property_number']; ?></td>
    <td><?php echo $list['unit_of_measure']; ?></td>
    <td><?php echo $list['unit_value']; ?></td>
    <td><?php echo $list['quantity_per_property_card']; ?></td>
    <td><?php echo $list['quantity_per_physical_count']; ?></td>
    <td><?php echo $list['shortage_coverage_quantity']; ?></td>
    <td><?php echo $list['shortage_coverage_value']; ?></td>
    <td><?php echo $list['remarks']; ?></td>
</tr>
<?php endforeach; ?>
</table>
</page>
