<?php
session_start();
    // print " [From View: " . $__APP__ ."] ";
    require_once('constant.php');
    require_once('controller/users.php');

    $UsersController = new UsersController();
    if (isset($_POST['submit'])) {
      unset($_POST['submit']);
      $details = $UsersController->getLogin($_POST);
      if ($details) {
          $_SESSION['loginUser'] = $details[0];
          $location = "Location: index.php";
          header($location);
      }
  }

    require_once('header.php');
?>
<style>
body {
  display: -ms-flexbox;
  display: flex;
  -ms-flex-align: center;
  align-items: center;
  padding-top: 40px;
  padding-bottom: 40px;
  background-color: #f5f5f5;
}

.form-signin {
  width: 100%;
  max-width: 330px;
  padding: 15px;
  margin: auto;
}
.form-signin .checkbox {
  font-weight: 400;
}
.form-signin .form-control {
  position: relative;
  box-sizing: border-box;
  height: auto;
  padding: 10px;
  font-size: 16px;
}
.form-signin .form-control:focus {
  z-index: 2;
}
.form-signin input[type="email"] {
  margin-bottom: -1px;
  border-bottom-right-radius: 0;
  border-bottom-left-radius: 0;
}
.form-signin input[type="password"] {
  margin-bottom: 10px;
  border-top-left-radius: 0;
  border-top-right-radius: 0;
}
</style>
<?php
    require_once('header-end.php');
    require_once('navigation.php');
?>
<main role="main" class="container">
<form class="form-signin" method="POST" action="<?php echo $_SERVER['PHP_SELF']; ?>">
  <img class="mb-4" src="/docs/4.4/assets/brand/bootstrap-solid.svg" alt="" width="72" height="72">
  <h1 class="h3 mb-3 font-weight-normal">Please sign in</h1>
  <label for="inputEmail" class="sr-only">Email address</label>
  <input name=username type="email" id="inputEmail" class="form-control" placeholder="Email address" required autofocus>
  <label for="inputPassword" class="sr-only">Password</label>
  <input name=password type="password" id="inputPassword" class="form-control" placeholder="Password" required>
  
  <button class="btn btn-lg btn-primary btn-block" type="submit" name="submit">Sign in</button>
</form>

</main><!-- /.container -->
<?php
    require_once('footer.php');