-- phpMyAdmin SQL Dump
-- version 5.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Mar 09, 2020 at 12:23 PM
-- Server version: 10.4.11-MariaDB
-- PHP Version: 7.4.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `db_thesis`
--

-- --------------------------------------------------------

--
-- Table structure for table `test_table`
--

CREATE TABLE `tools_and_equipments` (
    `id` int(11) NOT NULL,
    `report_id` int(11) NOT NULL,
    `article` varchar(300) NULL,
    `description` text DEFAULT NULL,
    `property_number` varchar(300) NULL,
    `unit_of_measure` varchar(300) NULL,
    `unit_value` varchar(300) NULL,
    `quantity_per_property_card` varchar(300) NULL,
    `quantity_per_physical_count` varchar(300) NULL,
    `shortage_coverage_quantity` varchar(300) NULL,
    `shortage_coverage_value` varchar(300) NULL,
    `remarks` text DEFAULT NULL,
    `is_deleted` Boolean DEFAULT false,
    `created_at` datetime NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Indexes for table `test_table`
--
ALTER TABLE `tools_and_equipments`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `test_table`
--
ALTER TABLE `tools_and_equipments`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
