<?php
session_start();
error_reporting(0);
ini_set('display_errors', 0);

require_once('../constant.php');
require __DIR__.'/../vendor/autoload.php';
require_once(__DIR__.'/../controller/tools_and_equipments.php');

use Spipu\Html2Pdf\Html2Pdf;
$ToolsAndEquipmentsController = new ToolsAndEquipmentsController();

ob_start();
$lists = $ToolsAndEquipmentsController->get(null, 1, 1, true);
require dirname(__FILE__).'/../template/pdf/tools-and-equipments.php';
$content = ob_get_clean();

$html2pdf = new Html2Pdf('L', 'A4', 'en');
$html2pdf->pdf->SetDisplayMode('fullpage');
// $html2pdf->writeHTML('<h1>HelloWorld</h1>This is my first test');
$html2pdf->writeHTML($content);
# $html2pdf->output("test.pdf", "D");
$html2pdf->output("test.pdf");