<?php
session_start();
    // print " [From View: " . $__APP__ ."] ";
    require_once('../constant.php');
    require_once('../controller/tools_and_equipments.php');

    $ToolsAndEquipmentsController = new ToolsAndEquipmentsController();
    $report_id = (int)$_GET['report-id'];
    // $ToolsAndEquipmentsController->add();
    // echo "<pre>";
    // print_r($_POST);
    // echo "</pre>";
    if (isset($_POST['submit'])) {
        unset($_POST['submit']);
        $id = $ToolsAndEquipmentsController->add($_POST);
        if (0 <= $id) {
            $location = "Location: ../reports/view.php?id={$report_id}";
            header($location);
        }
    }
   
    require_once('../header.php');
    require_once('../header-end.php');
    require_once('../navigation.php');
?>
<main role="main" class="container">

<form method="POST" action="?report-id=<?php echo $report_id; ?>">
    <div class="form-group">
        <label for="articleInput">Article</label>
        <input name="report_id" type="hidden" class="form-control" id="reportIdImport" value="<?php echo $report_id; ?>">
        <input name="article" type="text" class="form-control" id="articleInput">
    </div>
    <div class="form-group">
        <label for="descriptionTextArea">Description</label>
        <textarea name="description" class="form-control" id="descriptionTextArea" rows="3"></textarea>
    </div>
    <div class="form-group">
        <label for="propertyNumber">Property Number</label>
        <input name="property_number" type="text" class="form-control" id="propertyNumber">
    </div>
    <div class="form-group">
        <label for="unitOfMeasure">Unit of Measure</label>
        <input name="unit_of_measure" type="text" class="form-control" id="unitOfMeasure">
    </div>
    <div class="form-group">
        <label for="unitValue">Unit Value</label>
        <input name="unit_value" type="text" class="form-control" id="unitValue">
    </div>
    <div class="form-group">
        <label for="quantityPerPropertyCard">Quantity per Property Card</label>
        <input name="quantity_per_property_card" type="text" class="form-control" id="quantityPerPropertyCard">
    </div>
    <div class="form-group">
        <label for="quantityPerPhysicalCount">Quantity per Physical Count</label>
        <input name="quantity_per_physical_count" type="text" class="form-control" id="quantityPerPhysicalCount">
    </div>
    <div class="form-group">
        <label for="shortageCoverageQuantity">Shortage Coverange Quantity</label>
        <input name="shortage_coverage_quantity" type="text" class="form-control" id="shortageCoverageQuantity">
    </div>
    <div class="form-group">
        <label for="shortageCoverageValue">Shortage Coverange Value</label>
        <input name="shortage_coverage_value" type="text" class="form-control" id="shortageCoverageValue">
    </div>
    <div class="form-group">
        <label for="remarksTextArea">Remarks</label>
        <textarea name="remarks" class="form-control" id="remarksTextArea" rows="3"></textarea>
    </div>
    <button name="submit" type="submit" class="btn btn-primary">Submit</button>
    <a class="btn btn-primary" href="../tools-and-equipments" role="button">Cancel</a>
</form>


</main><!-- /.container -->
<?php
    require_once('../footer.php');
