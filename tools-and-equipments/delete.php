<?php
session_start();
    // print " [From View: " . $__APP__ ."] ";
    require_once('../constant.php');
    require_once('../controller/tools_and_equipments.php');

    $ToolsAndEquipmentsController = new ToolsAndEquipmentsController();
    $report_id = (int)$_GET['report-id'];
    if (isset($_POST['submit'])) {
        unset($_POST['submit']);
        $id = $ToolsAndEquipmentsController->remove($_GET['id']);
        if (0 <= $id) {
            header("Location: ../reports/view.php?id={$report_id}&deleted=true");
        }
    }

    $details = $ToolsAndEquipmentsController->get((int)$_GET['id']);
    if (empty($details)) {
        header("Location: ../reports/view.php?id={$report_id}");
    }
    $detail = $details['data'][0];
   
    require_once('../header.php');
    require_once('../header-end.php');
    require_once('../navigation.php');
?>
<main role="main" class="container">

<span class="badge badge-danger">Are you sure you want to DELETE this data?</span>
<form actoin="?report-id=<?php echo $report_id; ?>&id=<?php echo $_GET['id']; ?>" method="POST">
    <div class="form-group">
        <label for="articleInput">Article</label>
        <input disabled name="article" type="text" class="form-control" id="articleInput" value="<?php echo $detail['article'];?>">
    </div>
    <div class="form-group">
        <label for="descriptionTextArea">Description</label>
        <textarea disabled name="description" class="form-control" id="descriptionTextArea" rows="3"><?php echo $detail['description'];?></textarea>
    </div>
    <div class="form-group">
        <label for="propertyNumber">Property Number</label>
        <input disabled name="property_number" type="text" class="form-control" id="propertyNumber" value="<?php echo $detail['property_number'];?>">
    </div>
    <div class="form-group">
        <label for="unitOfMeasure">Unit of Measure</label>
        <input disabled name="unit_of_measure" type="text" class="form-control" id="unitOfMeasure" value="<?php echo $detail['unit_of_measure'];?>">
    </div>
    <div class="form-group">
        <label for="unitValue">Unit Value</label>
        <input disabled name="unit_value" type="text" class="form-control" id="unitValue" value="<?php echo $detail['unit_value'];?>">
    </div>
    <div class="form-group">
        <label for="quantityPerPropertyCard">Quantity per Property Card</label>
        <input disabled name="quantity_per_property_card" type="text" class="form-control" id="quantityPerPropertyCard" value="<?php echo $detail['quantity_per_property_card'];?>">
    </div>
    <div class="form-group">
        <label for="quantityPerPhysicalCount">Quantity per Physical Count</label>
        <input disabled name="quantity_per_physical_count" type="text" class="form-control" id="quantityPerPhysicalCount" value="<?php echo $detail['quantity_per_physical_count'];?>">
    </div>
    <div class="form-group">
        <label for="shortageCoverageQuantity">Shortage Coverange Quantity</label>
        <input disabled name="shortage_coverage_quantity" type="text" class="form-control" id="shortageCoverageQuantity" value="<?php echo $detail['shortage_coverage_quantity'];?>">
    </div>
    <div class="form-group">
        <label for="shortageCoverageValue">Shortage Coverange Value</label>
        <input disabled name="shortage_coverage_value" type="text" class="form-control" id="shortageCoverageValue" value="<?php echo $detail['shortage_coverage_value'];?>">
    </div>
    <div class="form-group">
        <label for="remarksTextArea">Remarks</label>
        <textarea disabled name="remarks" class="form-control" id="remarksTextArea" rows="3"><?php echo $detail['remarks'];?></textarea>
    </div>
    <button name="submit" type="submit" class="btn btn-primary">Delete</button>
    <a class="btn btn-primary" href="../reports/view.php?id=<?php echo $report_id; ?>" role="button">Cancel</a>
</form>


</main><!-- /.container -->
<?php
    require_once('../footer.php');
