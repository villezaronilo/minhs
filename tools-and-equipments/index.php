<?php
session_start();
    // print " [From View: " . $__APP__ ."] ";
    require_once('../constant.php');
    require_once('../controller/tools_and_equipments.php');

    $ToolsAndEquipmentsController = new ToolsAndEquipmentsController();
    $limit = (empty($_GET['limit'])) ? 10 : (int) $_GET['limit'];
    $page = (empty($_GET['page'])) ? 1 : (int) $_GET['page'];
    // echo "<pre>";
    // print_r($_GET['limit']);
    // print_r($limit);
    // echo "</pre>";

    $lists = $ToolsAndEquipmentsController->get(null, $limit, $page);
    $item_count = $lists['count'];

    // getting pageination
    $page_count = $item_count / $limit;
    $page_modulus = $item_count % $limit;
    $page_count = (0 < $page_modulus) ? (int)$page_count + 1: $page_count;

    require_once('../header.php');
    require_once('../header-end.php');
    require_once('../navigation.php');
?>
<main role="main" class="container">

<a class="btn btn-primary" href="add.php" role="button">Add Item</a> | <a class="btn btn-primary" href="pdf.php" role="button">Download PDF</a>
<table class="table table-striped">
  <thead class="thead-dark">
    <tr>
        <th scope="col">Article</th>
        <th scope="col">Description</th>
        <th scope="col">Property Number</th>
        <th scope="col">Unit of Measure</th>
        <th scope="col">Unit Value</th>
        <th scope="col">Quantity per Property Card</th>
        <th scope="col">Quantity per Physical Count</th>
        <th scope="col">Shortage Coverage Quantity</th>
        <th scope="col">Shortage Coverage Value</th>
        <th scope="col">Remarks</th>
        <th scope="col">Action</th>
    </tr>
  </thead>
  <tbody>

    <?php foreach ($lists['data'] as $list): ?>
    <tr>
        <td><?php echo "{$list['article']}"; ?></td>
        <td><?php echo "{$list['description']}"; ?></td>
        <td><?php echo "{$list['property_number']}"; ?></td>
        <td><?php echo "{$list['unit_of_measure']}"; ?></td>
        <td><?php echo "{$list['unit_value']}"; ?></td>
        <td><?php echo "{$list['quantity_per_property_card']}"; ?></td>
        <td><?php echo "{$list['quantity_per_physical_count']}"; ?></td>
        <td><?php echo "{$list['shortage_coverage_quantity']}"; ?></td>
        <td><?php echo "{$list['shortage_coverage_value']}"; ?></td>
        <td><?php echo "{$list['remarks']}"; ?></td>
        <td>
            <a href="edit.php?id=<?php echo $list['id'];?>">Edit</a>
            <a href="delete.php?id=<?php echo $list['id'];?>">Delete</a>
        </td>
    </tr>
    <?php endforeach; ?>

  </tbody>
</table>
<nav aria-label="Page navigation example">
  <ul class="pagination">
    <?php for ($i=1;$i<=$page_count;$i++): ?>
        <li class="page-item"><a class="page-link" href="?page=<?php echo $i; ?>&limit=<?php echo $limit; ?>"><?php echo $i; ?></a></li>
    <?php endfor; ?>
  </ul>
</nav>


</main><!-- /.container -->
<?php
    require_once('../footer.php');
